package com.luxoft.rmq.controller;

import com.luxoft.rmq.domain.Order;
import com.luxoft.rmq.service.ExceptionsManager;
import com.luxoft.rmq.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DemoController {

    private final OrderService orderService;
    private final ExceptionsManager exceptionsManager;

    @GetMapping("/toggle-exceptions")
    public ResponseEntity<String> toggleExceptions() {
        exceptionsManager.toggleExceptions();
        return exceptionsManager.shouldThrowExceptions()
                ? ok("Throwing exceptions")
                : ok("Correctly processing");
    }

    @PostMapping("/orders")
    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
        this.orderService.createOrder(order);
        return ok(order);
    }
}
