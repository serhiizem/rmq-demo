package com.luxoft.rmq.listener;

import com.luxoft.rmq.service.ExceptionsManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderCreationListener {

    private final ExceptionsManager exceptionsManager;
    private final DelayService delayService;

    @RabbitListener(queues = "${orders_queue}", containerFactory = "consumerContainerFactory")
    public void onOrderCreation(Message message) {
        if (exceptionsManager.shouldThrowExceptions()) {
            log.info("Going to throw exceptions");
            simulateErrorBehavior(message);
        } else {
            log.info("Successfully processing message: {}", message);
        }
    }

    private void simulateErrorBehavior(Message message) {
        try {
            throw new IllegalArgumentException("Simulated error");
        } catch (Exception e) {
            //avoid excessive logging for demo
            delayService.resendWithDelayOrIgnore(message);
        }
    }
}
