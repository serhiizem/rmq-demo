package com.luxoft.rmq.listener;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Slf4j
@Service
@RequiredArgsConstructor
public class DelayService {

    private static final int MAXIMUM_NUMBER_OF_RETRIES = 3;
    private static final String RETRY_NUMBER_HEADER = "x-retry";
    private static final Map<Integer, Integer> DELAY_MAPPING = ImmutableMap.<Integer, Integer>builder()
            .put(1, 10_000)
            .put(2, 15_000)
            .put(MAXIMUM_NUMBER_OF_RETRIES, 20_000)
            .build();

    @Value("${delayed_exchange}")
    private String delayExchangeName;

    private final AmqpTemplate amqpTemplate;

    public void resendWithDelayOrIgnore(Message message) {
        MessageProperties messageProperties = message.getMessageProperties();
        Map<String, Object> headers = messageProperties.getHeaders();
        int retryNumber = parseInt(headers.computeIfAbsent(RETRY_NUMBER_HEADER, k -> 1).toString());

        if (retryNumber <= MAXIMUM_NUMBER_OF_RETRIES) {
            Integer delay = DELAY_MAPPING.get(retryNumber);
            printDelay(delay);

            messageProperties.setHeader(RETRY_NUMBER_HEADER, ++retryNumber);
            messageProperties.setDelay(delay);

            amqpTemplate.convertAndSend(delayExchangeName, "", message);
        } else {
            log.error("Exceeded the number of retry attempts");
        }
    }

    private void printDelay(Integer delay) {
        log.info("Delaying the resend for {} seconds", MILLISECONDS.toSeconds(delay));
    }
}
