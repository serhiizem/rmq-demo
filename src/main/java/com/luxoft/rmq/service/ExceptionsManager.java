package com.luxoft.rmq.service;

import org.springframework.stereotype.Service;

@Service
public class ExceptionsManager {

    private Boolean exceptionsToggle = Boolean.FALSE;

    public void toggleExceptions() {
        exceptionsToggle = !exceptionsToggle;
    }

    public boolean shouldThrowExceptions() {
        return exceptionsToggle;
    }
}
