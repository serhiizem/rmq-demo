package com.luxoft.rmq.service;

import com.google.gson.Gson;
import com.luxoft.rmq.domain.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {

    @Value("${orders_exchange}")
    private String exchangeName;

    private final AmqpTemplate amqpTemplate;
    private final Gson gson = new Gson();

    public void createOrder(Order order) {
        amqpTemplate.convertAndSend(exchangeName, "", gson.toJson(order));
    }
}
