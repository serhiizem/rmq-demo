package com.luxoft.rmq.domain;

import lombok.Value;

@Value
public class Order {
    String currency;
    String value;
}
