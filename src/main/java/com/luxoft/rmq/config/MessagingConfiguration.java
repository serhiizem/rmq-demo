package com.luxoft.rmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class MessagingConfiguration {

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${orders_queue}")
    private String queueName;

    @Value("${orders_exchange}")
    private String exchangeName;

    @Value("${delayed_exchange}")
    private String delayedExchangeName;

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory consumerContainerFactory(
            ConnectionFactory connectionFactory
    ) {
        SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory =
                new SimpleRabbitListenerContainerFactory();
        simpleRabbitListenerContainerFactory.setConnectionFactory(connectionFactory);
        simpleRabbitListenerContainerFactory.setDefaultRequeueRejected(false);
        return simpleRabbitListenerContainerFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }

    @Bean
    public Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    public FanoutExchange ordersExchange() {
        return new FanoutExchange(exchangeName);
    }

    @Bean
    public Binding ordersBinding(
            Queue queue,
            FanoutExchange ordersExchange
    ) {
        return BindingBuilder.bind(queue).to(ordersExchange);
    }

    @Bean
    public Exchange configureDelayExchange(
            AmqpAdmin amqpAdmin,
            Queue queue
    ) {
        DelayExchange delayExchange = new DelayExchange(delayedExchangeName, "fanout");
        amqpAdmin.declareExchange(delayExchange);
        amqpAdmin.declareBinding(BindingBuilder.bind(queue).to(delayExchange).with("").noargs());
        return delayExchange;
    }

}

