package com.luxoft.rmq.config;

import com.google.common.collect.ImmutableMap;
import org.springframework.amqp.core.Exchange;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class DelayExchange implements Exchange {

    private final String name;
    private final Map<String, Object> arguments;

    public DelayExchange(String name, String type) {
        this.name = name;
        this.arguments = ImmutableMap.<String, Object>builder()
                .put("x-delayed-type", type)
                .build();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return "x-delayed-message";
    }

    @Override
    public boolean isDurable() {
        return false;
    }

    @Override
    public boolean isAutoDelete() {
        return false;
    }

    @Override
    public Map<String, Object> getArguments() {
        return arguments;
    }

    @Override
    public boolean isDelayed() {
        return false;
    }

    @Override
    public boolean isInternal() {
        return false;
    }

    @Override
    public boolean shouldDeclare() {
        return true;
    }

    @Override
    public Collection<?> getDeclaringAdmins() {
        return Collections.emptyList();
    }

    @Override
    public boolean isIgnoreDeclarationExceptions() {
        return false;
    }
}
